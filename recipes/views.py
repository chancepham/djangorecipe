from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from django.http import HttpResponse
from .forms import RecipeForm  # Import your form class here
from django.contrib.auth.decorators import login_required

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)

    context = {
        "recipe_object": recipe,
        "form": form,
    }
    return render(request, "recipes/edit.html", context)

@login_required
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)  # Initialize the form with data from the request
        if form.is_valid():
            # If the form is valid, save the new thing to the database
            recipe = form.save(False)
            request.author = request.user
            recipe.save()
            # Redirect to another page, perhaps the detail view of the object
            return redirect('recipe_list')  # Adjust redirect as needed
    else:
        # If it's a GET request, create a new blank form
        form = RecipeForm()

    # Put the form in the context dictionary to pass to the template
    context = {'form': form}
    # Render the HTML template with the context
    return render(request, 'recipes/create.html', context)

# Create your views here.
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)


def show_recipe(request,id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context )


def custom_404_view(request, exception=None):

    return render(request, '404.html', status=404)
def redirect_to_recipe_list(request):
    return redirect("recipe_list")
