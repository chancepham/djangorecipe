"""
URL configuration for scrumptious project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import handler404
from django.urls import path, include  # Correct import
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
from django.urls import path, include
from recipes.views import custom_404_view,redirect_to_recipe_list

from django.conf import settings
from django.conf.urls.static import static

# if settings.DEBUG:
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns = [
    path('', redirect_to_recipe_list, name='home_page'),
    path('admin/', admin.site.urls),
    path('recipes/', include("recipes.urls")),
    path("accounts/", include("accounts.urls")),
]

# handler404 = 'recipes.views.custom_404_view'
# if settings.DEBUG:
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     # Override default error handlers in development
#     from django.conf.urls import url
#     from django.views.defaults import server_error

#     urlpatterns += [
#         url(r'^404/$', custom_404_view),
#         url(r'^500/$', server_error),
    # ]
